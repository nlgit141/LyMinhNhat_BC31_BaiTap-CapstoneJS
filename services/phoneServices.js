

const BASE_URL = "https://62b07883196a9e98702448f3.mockapi.io/Products";


export let phoneServices = {
    getDataPhone: () => {
        return axios({
            url: BASE_URL,
            method: "GET",
        });
    },
    removeSP: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "DELETE",
        });
    },
    addSP: (SP) => {
        return axios({
            url: BASE_URL,
            method: "POST",
            data: SP
        });
    },
    layChiTietSP: (idSP) => {
        return axios({
            url: `${BASE_URL}/${idSP}`,
            method: "GET",
        });
    },
    capNhatSP: (SP) => {
        return axios({
            url: `${BASE_URL}/${SP.id}`,
            method: "PUT",
            data: SP
        });
    }
};